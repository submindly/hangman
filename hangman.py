import turtle
import sys


def goto(x, y):
    turtle.penup()
    turtle.goto(x, y)
    turtle.pendown()


def line(from_x, from_y, to_x, to_y):
    goto(from_x, from_y)
    turtle.goto(to_x, to_y)


count = 0


def getstatus(word):
    # текущий шаг
    return [0, ['p', 'r', 'i', 'm', 'e', 'r'], 6, count]


coord_list = []
coord = [
    [-160, -100, -160, 80],
    [-160, 80, -80, 80],
    [-160, 40, -120, 80],
    [-100, 80, -100, 40],
    [-100, 0, -100, -50],
    [-100, -10, -100, -50],
    [-100, -10, -120, -20],
    [-100, -10, -80, -20],
    [-100, -50, -120, -60],
    [-100, -50, -80, -60]
]

for line in coord:
    line = line.strip().split(',')
    nums = []
    for n in line:
        nums.append(int(n))
    coord_list.append(nums)


while True:
    inputLetter = turtle.textinput("Введите букву", "А-я")
    letter = getstatus(inputLetter)
    status = letter[0]
    word = letter[1]
    allSteps = letter[2]
    step = letter[3]

    if status == 1:
        turtle.color('green')
        goto(-150, 200)
        turtle.write("Ура, вы победили", font=("Arial", 28, "normal"))
        break

    if status == 2:
        turtle.color('red')
        goto(-50, 200)
        turtle.write("Вы проиграли", font=("Arial", 44, "normal"))
        break

    else:
        if step == 4:
            turtle.color('red')
            goto(-150, 200)
            turtle.write("Неверно", font=("Arial", 28, "normal"))
            goto(-100, 0)
            turtle.circle(20)

        else:
            turtle.color('red')
            goto(-150, 200)
            turtle.write("Неверно", font=("Arial", 28, "normal"))
            line(*coord_list[step])
        turtle.color('red')
        goto(-150, 200)
        turtle.write("Неверно", font=("Arial", 28, "normal"))

input('Нажмите любую клавишу')
